This is a very simple offline end-to-end encrypted secret exchange app using Web Crypto API, Angular 8 and Angular Material.
See a working version hosted as a Gitlab page at: https://rkretzschmar.gitlab.io/ng-ssx/
Note: Unfortunately it is not running on IE or Microsoft Edge at the moment, even with the TextEncoding polyfill - see 7919579 and 12782429.
