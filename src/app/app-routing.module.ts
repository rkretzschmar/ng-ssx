import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { DecryptComponent } from './decrypt/decrypt.component';
import { EncryptComponent } from './encrypt/encrypt.component';
import { PublicKeyComponent } from './public-key/public-key.component';

const routes: Routes = [
  { path: '', redirectTo: '/public-key', pathMatch: 'full' },
  { path: 'encrypt', component: EncryptComponent },
  { path: 'encrypt/:publicKey', component: EncryptComponent },
  { path: 'decrypt', component: DecryptComponent },
  { path: 'public-key', component: PublicKeyComponent },
  { path: 'about', component: AboutComponent },
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes), CommonModule],
  exports: [RouterModule],
})
export class AppRoutingModule {}
