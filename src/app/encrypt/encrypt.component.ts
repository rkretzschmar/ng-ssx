import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { CryptoService } from '../crypto.service';

function sanitizePublicKey(s: string): string {
  return s.replace(/.*encrypt\//g, '');
}

@Component({
  selector: 'app-encrypt',
  templateUrl: './encrypt.component.html',
  styleUrls: ['./encrypt.component.scss'],
})
export class EncryptComponent implements OnInit {
  constructor(
    private cryptoService: CryptoService,
    private route: ActivatedRoute
  ) {}

  publicKeyValue: string;
  textValue: string;
  encryptedText: string;
  error: Error;

  @Output() publicKeyChange = new EventEmitter<string>();
  @Output() textChange = new EventEmitter<string>();

  @Input()
  get publicKey(): string {
    return this.publicKeyValue;
  }

  set publicKey(v: string) {
    this.publicKeyValue = v;
    this.publicKeyChange.emit(this.publicKeyValue);
  }

  @Input()
  get text(): string {
    return this.textValue;
  }

  set text(v: string) {
    this.textValue = v;
    this.textChange.emit(this.textValue);
  }

  async onEncrypt() {
    this.error = null;
    try {
      const publicKey = await this.cryptoService.importPublicKey(
        JSON.parse(atob(sanitizePublicKey(this.publicKeyValue)))
      );
      this.encryptedText = btoa(
        await this.cryptoService.encryptMessage(this.text, publicKey)
      );
    } catch (e) {
      this.error = e;
    }
  }

  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.publicKey = params.get('publicKey');
    });
  }
}
