import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CryptoService } from './crypto.service';

export interface NavLink {
  label: string;
  link: string;
  index: number;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'Simple Secret Exchange';
  navLinks: NavLink[];
  activeLinkIndex = -1;

  constructor(private cryptoService: CryptoService, private router: Router) {
    this.navLinks = [
      { label: 'Encrypt', link: './encrypt', index: 0 },
      { label: 'Decrypt', link: './decrypt', index: 1 },
      { label: 'Your Public Key', link: './public-key', index: 2 },
      { label: 'About', link: './about', index: 3 },
    ];
  }
  ngOnInit(): void {
    this.cryptoService.startUp();
    this.router.events.subscribe(_ => {
      this.activeLinkIndex = this.navLinks.indexOf(
        this.navLinks.find(tab => tab.link === `.${this.router.url}`)
      );
    });
  }
}
