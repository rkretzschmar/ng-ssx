import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CryptoService } from '../crypto.service';

@Component({
  selector: 'app-decrypt',
  templateUrl: './decrypt.component.html',
  styleUrls: ['./decrypt.component.scss'],
})
export class DecryptComponent {
  encryptedTextValue: string;
  decryptedText: string;
  error: Error;

  @Output() encryptedTextChange = new EventEmitter<string>();

  @Input()
  get encryptedText() {
    return this.encryptedTextValue;
  }

  set encryptedText(v: string) {
    this.encryptedTextValue = v;
    this.encryptedTextChange.emit(this.encryptedTextValue);
  }

  constructor(private cryptoService: CryptoService) {}

  async onDecrypt() {
    this.error = null;

    try {
      this.decryptedText = await this.cryptoService.decryptMessage(
        atob(this.encryptedText)
      );
    } catch (e) {
      this.error = e;
    }
  }
}
