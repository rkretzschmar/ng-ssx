import { TestBed } from '@angular/core/testing';
import { CryptoService } from './crypto.service';

describe('CryptoService', () => {
  let service: CryptoService;

  beforeEach(() => {
    window.localStorage.clear();
    TestBed.configureTestingModule({});
    service = TestBed.get(CryptoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should encrypt and decrypt UTF-8 strings', async (done: any) => {
    const text = 'äöüabcdefghijklmnopqrst: 😀+🥶=🧠';
    await service.startUp();

    service.publicKey$.subscribe(async (publicKey: CryptoKey) => {
      const encrypted = await service.encryptMessage(text, publicKey);
      const decrypted = await service.decryptMessage(encrypted);
      expect(decrypted).toBe(text);
      done();
    });
  });
});
