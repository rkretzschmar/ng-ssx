import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import { CryptoService } from '../crypto.service';

@Component({
  selector: 'app-public-key',
  templateUrl: './public-key.component.html',
  styleUrls: ['./public-key.component.scss'],
})
export class PublicKeyComponent implements OnInit {
  publicKey$: Observable<string>;
  encryptURL$: Observable<string>;
  publicKeyHash$: Observable<string>;

  constructor(
    private cryptoService: CryptoService,
    private route: ActivatedRoute
  ) {
    this.publicKey$ = cryptoService.publicKeyBase64$;
    const url$ = this.route.url.pipe(
      map(
        u =>
          window.location.origin +
          window.location.pathname.replace(`${u}`, '') +
          'encrypt'
      )
    );
    this.encryptURL$ = this.publicKey$.pipe(
      mergeMap((key: string) => {
        return url$.pipe(map(v => `${v}/${key}`));
      })
    );
    this.publicKeyHash$ = cryptoService.publicKeyHash$;
  }

  ngOnInit(): void {}

  // ngOnInit(): void {
  //   this.cryptoService.publicKey$.subscribe((publicKey: string) => {
  //     this.publicKey = publicKey;
  //     this.encryptURL = `${window.location.origin}${
  //       window.location.pathname
  //     }/encrypt/${this.publicKey}`;
  //     this.encryptEmailHref = `mailto:?subject=Send me a secure message&body=Send me a secure message by using this link: ${
  //       this.encryptURL
  //     }`;

  //     this.publicKeyHash = this.cryptoService.publicKeyHash;
  //   });
  // }

  async onCreateNewKey() {
    // this.publicKey = 'generating new key...';
    // this.publicKeyHash = '';
    this.cryptoService.generateNewKey();
  }
}
