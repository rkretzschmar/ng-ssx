import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatTabsModule } from '@angular/material/tabs';
import { By } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { ClipboardModule } from 'ngx-clipboard';
import { defer } from 'rxjs';
import { CryptoService } from '../crypto.service';
import { PublicKeyComponent } from './public-key.component';

const testPublicKey = '0123456789abcdef';
const testPublicKeyHash = '#0123456789abcdef';

const cryptoServiceMock = {
  publicKeyBase64$: defer(() => Promise.resolve(testPublicKey)),
  publicKeyHash$: defer(() => Promise.resolve(testPublicKeyHash)),
};

describe('PublicKeyComponent', () => {
  let component: PublicKeyComponent;
  let fixture: ComponentFixture<PublicKeyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PublicKeyComponent],
      imports: [
        MatButtonModule,
        MatTabsModule,
        MatIconModule,
        MatCardModule,
        MatInputModule,
        MatFormFieldModule,
        MatProgressBarModule,
        ClipboardModule,
        RouterModule.forRoot([]),
      ],
      providers: [{ provide: CryptoService, useValue: cryptoServiceMock }],
    }).compileComponents();
    fixture = TestBed.createComponent(PublicKeyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  beforeEach(() => {});

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render the public from the crypto service', async(async () => {
    await fixture.whenStable();
    fixture.detectChanges();
    expect(
      fixture.debugElement.nativeElement.querySelector('p').textContent
    ).toContain(testPublicKey);
    await fixture.whenStable();
    fixture.detectChanges();
    expect(
      fixture.debugElement.query(By.css('#fingerprint')).nativeElement
        .textContent
    ).toContain(testPublicKeyHash);
  }));
});
